<?php

namespace Dhe\Cruisecompass;

class Cruisecompass
{
    public function greet(String $sName)
    {
        return 'Hi ' . $sName . '! How are you doing today?';
    }
}